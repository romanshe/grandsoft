package com.grandsoft;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberListPanel extends JPanel {

    private static final int PANEL_WIDTH = 300;
    private static final int PANEL_HEIGHT = 300;
    private static final double CONSTRAINTS_WEIGHTX = 0.5;
    private static final double CONSTRAINTS_WEIGHTY = 0.5;

    private static final int NUM_PER_COLUMN_COUNT = 5;
    private static final int NUM_MAX_VALUE = 100;
    private static final int MAX_VALUE_TO_GENERATE = 30;
    private static final int START_COLUMN = 0;

    private static final Random RANDOM = new Random();
    private static final int NUM_BELOW_MAX_VALUE_TO_GENERATE = 1;
    private GridBagConstraints constraints = new GridBagConstraints();

    private ArrayList<Integer> randomNum;
    private boolean sortOrder = false;

    private ActionListener generateNewNumbersListener = (event) -> {
        Integer number = Integer.valueOf(event.getActionCommand());
        if (number < MAX_VALUE_TO_GENERATE) {
            createPanel(number);
        } else {
            JOptionPane.showMessageDialog(null, "Please, chose number lower than 30.", "Warning", JOptionPane.INFORMATION_MESSAGE);
        }
    };

    public NumberListPanel() {
        Dimension size = getPreferredSize();
        size.setSize(PANEL_WIDTH, PANEL_HEIGHT);
        setPreferredSize(size);
        setLayout(new GridBagLayout());

        constraints.anchor = GridBagConstraints.PAGE_START;
        constraints.weightx = CONSTRAINTS_WEIGHTX;
        constraints.weighty = CONSTRAINTS_WEIGHTY;
    }

    public void createPanel(int numCount) {
        randomNum = generateRandomNumbers(numCount);
        drawPanel();
    }

    public void sortNumbers() {
        if (sortOrder) {
            Collections.sort(randomNum, Collections.reverseOrder());
        } else {
            Collections.sort(randomNum);
        }
        sortOrder = !sortOrder;
        drawPanel();
    }

    private void drawPanel() {
        removeAll();
        drawNumButtons();
        revalidate();
        repaint();
    }

    private void drawNumButtons() {
        int column = START_COLUMN;
        int numCount = randomNum.size();
        int totalColumnCount = numCount / NUM_PER_COLUMN_COUNT;
        int lastColumnNumCount = numCount % NUM_PER_COLUMN_COUNT;
        for (int j = 0; j < totalColumnCount; j++) {
            drawColumn(column++, randomNum.subList(j * NUM_PER_COLUMN_COUNT, (j + 1) * NUM_PER_COLUMN_COUNT));
        }
        if (lastColumnNumCount > 0) {
            drawColumn(column, randomNum.subList(totalColumnCount * NUM_PER_COLUMN_COUNT, numCount));
        }
    }

    private void drawColumn(int column, List<Integer> numList) {
        constraints.gridx = column;
        constraints.gridy = GridBagConstraints.RELATIVE;
        numList.stream().map(String::valueOf).forEach(this::addButton);
    }

    private void addButton(String btnTitle) {
        JButton btn = new JButton(btnTitle);
        btn.setActionCommand(btnTitle);
        btn.addActionListener(generateNewNumbersListener);
        add(btn, constraints);
    }

    private static ArrayList<Integer> generateRandomNumbers(int count) {
        ArrayList<Integer> list = Stream.generate(() -> RANDOM.nextInt(NUM_MAX_VALUE))
                .limit(count - NUM_BELOW_MAX_VALUE_TO_GENERATE)
                .collect(Collectors.toCollection(ArrayList::new));
        list.add(RANDOM.nextInt(MAX_VALUE_TO_GENERATE));
        return list;
    }
}
