package com.grandsoft;

import org.apache.commons.lang3.StringUtils;

import javax.swing.*;
import java.awt.*;

public class RandomNumbersApplication {
    private final static String MAIN_PAGE = "Main page";
    private final static String NUMBERS_PAGE = "Numbers page";
    private static final String BTN_RESET = "Reset";
    private static final String BTN_SORT = "Sort";
    private static final String BTN_ENTER = "Enter";
    private static final String TITLE = "Random numbers application";

    private JPanel pages;
    private JPanel mainPage;
    private JPanel numbersPage;
    private JPanel numbersControlPanel;
    private NumberListPanel numberListPanel;

    public void addComponentToPane(Container pane) {
        mainPage = createMainPage();
        numbersPage = createNumbersPage();

        pages = new JPanel(new CardLayout());
        pages.add(mainPage, MAIN_PAGE);
        pages.add(numbersPage, NUMBERS_PAGE);

        pane.add(pages, BorderLayout.PAGE_START);
    }

    private JPanel createMainPage() {
        JPanel mainPage = new JPanel();
        JTextField enterText = new JTextField(50);
        mainPage.add(enterText);
        JButton btnEnter = new JButton(BTN_ENTER);
        btnEnter.addActionListener( (event) -> {
                String userInput = enterText.getText();
                if (StringUtils.isNumeric(userInput)){
                    int numCount = Integer.valueOf(userInput);
                    numberListPanel.createPanel(numCount);
                } else {
                    JOptionPane.showMessageDialog(null, "Please enter an integer number.", "Warning", JOptionPane.INFORMATION_MESSAGE);
                }
                switchToPage(NUMBERS_PAGE);
            });
        mainPage.add(btnEnter);
        return mainPage;
    }

    private JPanel createNumbersPage() {
        JPanel numbersPage = new JPanel();
        numberListPanel = new NumberListPanel();
        numbersControlPanel = createNumbersControlPanel();
        numbersPage.add(numberListPanel);
        numbersPage.add(numbersControlPanel);
        return numbersPage;
    }

    private JPanel createNumbersControlPanel() {
        JPanel numbersControlPanel = new JPanel();

        JButton btnReset = new JButton(BTN_RESET);
        btnReset.addActionListener((event) -> switchToPage(MAIN_PAGE));
        numbersControlPanel.add(btnReset);

        JButton btnSort = new JButton(BTN_SORT);
        btnSort.addActionListener((event) -> numberListPanel.sortNumbers());
        numbersControlPanel.add(btnSort);

        return numbersControlPanel;
    }

    private void switchToPage(String pageName) {
        CardLayout cardLayout = (CardLayout) (pages.getLayout());
        cardLayout.show(pages, pageName);
    }

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame frame = new JFrame(TITLE);
                frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

                RandomNumbersApplication demo = new RandomNumbersApplication();
                demo.addComponentToPane(frame.getContentPane());

                frame.pack();
                frame.setVisible(true);
            }
        });
    }
}
